## WPezPlugins: Hubspot Newsletter Form via AJAX

__Full discloser: This is not a proper plugin per se. Instead, consider it boilerplate that you can fork and customize.__


### OVERVIEW

I had a client request to use Hubspot for newsletter blasts, as well as anything else HS that might be helpful. Unfortunately, the HS embedded forms couldn't be customized to my satisfaction. Instead, with approval, I decided to use a non-Hubspot form and add the submitted form data via the HS API. The result of that decision and work is this repo. 

There is also a sibling plugin for the actual form. It made better sense to "decouple" the form from this main plugin.

Finally, that form, along with the JS in this "plugin", often relies on CSS to show / hide elements and such. For this initial iteration, that CSS is currently in the theme.  I'll have to add that CSS here as well. 


### REFERENCES

- https://gitlab.com/WPezPlugins/wpez-hubspot-newsletter-form-ajax-form

- https://legacydocs.hubspot.com/docs/methods/forms/submit_form


### TODO

- Add the CSS to this repo as a reference point.

- Add filters to the Config, and elsewhere.

- In general, try to nudge it towards more of a proper plugin (and less forkable boilerplate).

- Add References links to HubSpot forum Q&As that were triggered by this effort. 


### CHANGE LOG

__-- 0.0.2 - Fri 26 Nov 2021__

- Added filter to be able to use a third-party spam prevention service (instead of e.g., CAPTCHA)
- Assorted other nips and tucks.


__-- 0.0.1 - Mon 1 Nov 2021__

- INIT - Hey! Ho!! Let's go!!!

   