<?php
/**
 * https://legacydocs.hubspot.com/docs/methods/contacts/create_contact
 * 
 * https://legacydocs.hubspot.com/docs/methods/forms/submit_form
 */

namespace WPezHNFA\App\Lib\HubspotFormsSubmission;



class ClassHubspotFormsSubmission {



	/**
	 * HS form's portal id.
	 *
	 * @var string
	 */
	protected $str_portal_id;

	/**
	 * HS form's form id.
	 *
	 * @var string
	 */
	protected $str_form_id;

	/**
	 * The api's endpoint (sans the portal id and form id).
	 *
	 * @var string
	 */
	protected $str_endpoint_base;

	/**
	 * The data being sent to HS is pushed on to this array.
	 *
	 * @var array
	 */
	protected $arr_args;



	public function __construct( string $portal_id = '', string $form_id = '' ) {

		$this->str_portal_id = trim( $portal_id );
		$this->str_form_id   = trim( $form_id );

		$this->setPropertyDefaults();

	}


	protected function setPropertyDefaults() {

		$this->arr_args = array();
		// TODO - added a setter for this?
		$this->str_endpoint_base = 'https://api.hsforms.com/submissions/v3/integration/submit/';
	}


	public function pushArgs( string $key = '', array $args = array() ) {

		if ( ! empty( $key ) ) {

			$this->arr_args[ trim( $key ) ] = $args;
		}

	}

	/**
	 * Posts to the HS endpoint
	 *
	 * @return string
	 */
	public function curlPost() {

		if ( ! empty( $this->arr_args ) && ! empty( $this->str_portal_id ) && ! empty( $this->str_form_id ) ) {

			$post_json = wp_json_encode( $this->arr_args );

			if ( is_string( $post_json ) ) {

				$endpoint = $this->str_endpoint_base . $this->str_portal_id . '/' . $this->str_form_id;

				$ch = @curl_init();
				@curl_setopt( $ch, CURLOPT_POST, true );
				@curl_setopt( $ch, CURLOPT_POSTFIELDS, $post_json );
				@curl_setopt( $ch, CURLOPT_URL, $endpoint );
				@curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json' ) );
				@curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
				$response = @curl_exec( $ch );
				$status_code = @curl_getinfo( $ch, CURLINFO_HTTP_CODE );
				$curl_errors = curl_error( $ch );
				@curl_close( $ch );

				return array(
					'errors'      => $curl_errors,
					'status_code' => $status_code,
					'response'    => $response,
				);
			}
			// function: wp_json_encode returned false.
			return array(
				'errors'      => '',
				'status_code' => 400,
				'response'    => '{"errors":[{"message":"Error in ClassHubspotFormsSubmission","errorType":"CURL_POST_2"}]}',
			);
		}
		// something is wrong with arr_args, portal_id and/or form_id.
		return array(
			'errors'      => '',
			'status_code' => 400,
			'response'    => '{"errors":[{"message":"Error in ClassHubspotFormsSubmission","errorType":"CURL_POST_1"}]}',
		);
	}
}
