<?php
/**
 * As inspired by:
 *
 * Https://code.tutsplus.com/tutorials/object-oriented-programming-in-wordpress-building-the-plugin-ii--cms-21105
 *
 * https://github.com/DevinVinson/WordPress-Plugin-Boilerplate/blob/master/plugin-name/includes/class-plugin-name-loader.php
 *
 * The notable ez improvement is loading is done with arrays, not (hardcoded) code.
 * The benefit is you can define your hooks and then - prior to loading -
 * slap a filter on those arrays to allow for customization by users (read:
 * other devs). As a result any hook'ed callback (method) can be replaced as
 * needed.
 *
 * Yeah. Cool :)
 *
 * @package WPezHNFA\App\Lib\HooksRegister
 */

namespace WPezHNFA\App\Lib\HooksRegister;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

/**
 * TODO - add comments, doc blocks, etc.
 */
class ClassHooksRegister {

	/**
	 * Actions to be registered.
	 *
	 * @var array
	 */
	protected $arr_actions;

	/**
	 * Filters to be registered.
	 *
	 * @var array
	 */
	protected $arr_filters;

	/**
	 * Defaults of the hooks.
	 *
	 * @var array
	 */
	protected $arr_hook_defaults;


	/**
	 * The constructor.
	 */
	public function __construct() {

		$this->setPropertyDefaults();

	}

	/**
	 * Sets the default value of the properties.
	 *
	 * @return void
	 */
	protected function setPropertyDefaults() {

		$this->arr_actions = array();
		$this->arr_filters = array();

		$this->arr_hook_defaults = array(
			'active'        => true,
			'hook'          => false,
			'component'     => false,
			'callback'      => false,
			'priority'      => 10,
			'accepted_args' => 1,
		);
	}

	/**
	 * Push a single action into the actions' array.
	 *
	 * @param array $arr_args Array of the action's args.
	 *
	 * @return bool
	 */
	public function pushAction( array $arr_args = array() ) {

		if ( empty( $arr_args ) ) {
			return false;
		}

		return $this->pushMaster( 'arr_actions', $arr_args );

	}

	/**
	 * Push a single filter into the filters' array.
	 *
	 * @param array $arr_args Array of the action's args.
	 *
	 * @return bool
	 */
	public function pushFilter( array $arr_args = array () ) {

		if ( empty( $arr_args ) ) {
			return false;
		}
		return $this->pushMaster( 'arr_filters', $arr_args );

	}


	/**
	 * Does the actual push for both types of hooks.
	 *
	 * @param string $str_prop The hook type, 'action' or 'filter'.
	 * @param array  $arr_args The hooks' args.
	 *
	 * @return bool
	 */
	protected function pushMaster( string $str_prop = '', array $arr_args = array() ){

		if ( empty( $arr_args ) ) {
			return false;
		}

		$obj_hook = (object) array_merge( $this->arr_hook_defaults, $arr_args );

		// maybe we have a problem?
		if ( true !== $obj_hook->active || false === $obj_hook->hook || false === $obj_hook->component || false === $obj_hook->callback ) {
			return false;
		}

		$this->$str_prop[] = $obj_hook;
		return true;

	}

	/**
	 * Undocumented function
	 *
	 * @param array $arr_arrs
	 * @return void
	 */
	public function loadActions( array $arr_arrs = array() ) {

		if ( empty( $arr_arrs ) ) {
			return false;
		}

		$this->loadMaster( 'pushAction', $arr_arrs );
		return true;

	}

	public function loadFilters( array $arr_arrs = array() ) {

		if ( empty( $arr_arrs ) ) {
			return false;
		}
		$this->loadMaster( 'pushFilter', $arr_arrs );
		return true;
	}


	protected function loadMaster( string $str_method = '', array $arr_arrs = array() ) {

		foreach ( $arr_arrs as $arr ) {

			$this->$str_method( $arr );
		}
	}


	public function doRegister( bool $bool_actions = true, bool $bool_filters = true ) {

		if ( true === $bool_actions && ! empty( $this->arr_actions ) ) {
			$this->register( 'add_action', $this->arr_actions );
		}

		if ( true === $bool_filters && ! empty( $this->arr_filters ) ) {
			$this->register( 'add_filter', $this->arr_filters );
		}
	}

	protected function register( string $str_wp_function = '', array $arr_objs = array() ) {

		foreach ( $arr_objs as $obj ) {
			$str_wp_function( trim( $obj->hook ), array( $obj->component, trim( $obj->callback ) ), $obj->priority, $obj->accepted_args );
		}
	}
}
