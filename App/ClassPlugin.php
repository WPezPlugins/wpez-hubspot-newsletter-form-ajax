<?php
/**
 * Class that coordinates the plugin's classes and hooks.
 *
 * @package WPezHNFA\App
 */

namespace WPezHNFA\App;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

use WPezHNFA\App\{
	Lib\HooksRegister\ClassHooksRegister as HooksRegister,
	Config\InterfaceConfig,
	Src\Filters\ClassFilters as Customize,
	Src\Setup\ClassSetup as Setup,
	Src\Setup\ClassAdminSettings as Settings,
	Src\ClassHubspotNewsletterFormAjax as HNFA,
};

/**
 * Coordinates the plugin's classes and hooks.
 */
class ClassPlugin {

	/**
	 * Instance of the ClassHooksRegister class.
	 *
	 * @var object
	 */
	protected $new_hooks_reg;

	/**
	 * Array of the plugin's actions.
	 *
	 * @var array
	 */
	protected $arr_actions;

	/**
	 * Array of the plugin's filters.
	 *
	 * @var array
	 */
	protected $arr_filters;


	protected $new_config;

	protected $new_setup;

	protected $new_settings;

	/**
	 * Instance of the primary / main class.
	 *
	 * @var object
	 */
	protected $new_main;

	/**
	 * Let's construct.
	 *
	 * @param Config $new_config Instance of the ClassConfig class.
	 */
	public function __construct( InterfaceConfig $new_config ) {

			$this->setPropertyDefaults( $new_config );

			$this->actions( true );

			$this->filters( 'TODO');

			// this must be last.
			$this->hooksRegister();
	}

	protected function setPropertyDefaults( $new_config ) {

		$this->new_config = $new_config;

		$this->new_hooks_reg = new HooksRegister();
		$this->arr_actions   = array();
		$this->arr_filters   = array();

		$this->new_setup = new Setup( $new_config );
		$this->new_settings = new Settings( $new_config );

		$new_customize   = new Customize( $new_config->getPluginSlug(), array() );
		$this->new_main  = new HNFA( $new_config, $new_customize );
	
	}


	/**
	 * After gathering (below) the arr_actions and arr_filter, it's time to
	 * make some RegisterHook magic
	 */
	protected function hooksRegister() {

		$this->new_hooks_reg->loadActions( $this->arr_actions );

		$this->new_hooks_reg->loadFilters( $this->arr_filters );

		$this->new_hooks_reg->doRegister();
	}

	/**
	 * Setup the plugin's actions. There are currently no actions, only filters.
	 *
	 * @param boolean $bool A flag for toggling the method.
	 *
	 * @return void
	 */
	public function actions( $bool = true ) {

		if ( true !== $bool ) {
			return;
		}

		$this->arr_actions[] = array(
			'active'    => true,
			'hook'      => 'wp_enqueue_scripts',
			'component' => $this->new_setup,
			'callback'  => 'enqueueJSMain',
			'priority'  => 10,
		);

		$this->arr_actions[] = array(
			'active'    => true,
			'hook'      => 'wp_enqueue_scripts',
			'component' => $this->new_setup,
			'callback'  => 'localizeJSMain',
			'priority'  => 20,
		);

		$str_ajax_action = trim( $this->new_config->getAjaxAction() );

		$this->arr_actions[] = array(
			'active'    => true, // active does not have to be set. it - in ClassRegisterHooks - is default: true
			'hook'      => 'wp_ajax_' . $str_ajax_action,
			'component' => $this->new_main,
			'callback'  => 'wpAjaxCallback',
			'priority'  => 10,
		);

		$this->arr_actions[] = array(
			'active' => true, // active does not have to be set. it - in ClassRegisterHooks - is default: true
			'hook' => 'wp_ajax_nopriv_' . $str_ajax_action,
			'component' => $this->new_main,
			'callback' => 'wpAjaxCallback',
			'priority' => 10,
		);

//	add_action( 'admin_menu', array( $new_plugin, 'add_submenu_page' ) );
//	add_action( 'admin_init', array( $new_plugin, 'display_settings' ) );


		$this->arr_actions[] = array(
			'active'    => true,
			'hook'      => 'admin_menu',
			'component' => $this->new_settings,
			'callback'  => 'addSubmenuPage',
			'priority'  => 20,
		);

		$this->arr_actions[] = array(
			'active'    => true,
			'hook'      => 'admin_init',
			'component' => $this->new_settings,
			'callback'  => 'displaySettings',
			'priority'  => 20,
		);

	}

	/**
	 * Setup the plugin's filters.
	 *
	 * @param boolean $bool A flag for toggling the method.
	 *
	 * @return void
	 */
	public function filters( $bool = true ) {

		if ( true !== $bool ) {
			return;
		}

		/**
		$this->arr_filters[] = array(
			'active'        => true,
			'hook'          => 'TODO',
			'component'     => $this->new_main,
			'callback'      => 'evaluateImg',
			'priority'      => 5,
			'accepted_args' => 2,
		);
		*/

	}
}
