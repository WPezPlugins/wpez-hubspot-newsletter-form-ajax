<?php
/**
 * The class that defines the settings / default values for the plugin.
 *
 * @package WPezHNFA\App\Config
 */

namespace WPezHNFA\App\Config;

/**
 * {@inheritDoc}
 */
class ClassConfig implements InterfaceConfig {

	/**
	 * The plugin's dir.
	 *
	 * @var string
	 */
	private $plugin_dir;

	/**
	 * The plugin's url.
	 *
	 * @var string
	 */
	private $plugin_url;

	/**
	 * The plugin's slug, implode()'ed with a separator.
	 *
	 * @var array
	 */
	private $plugin_slug;


	/**
	 * Array that stores the slugs, indexed by the separators.
	 *
	 * @var array
	 */
	private $arr_plugin_slugs;

	private $settings_prefix;

	private $settings_parent_menu_slug;

	private $settings_submenu_page_cap;

	private $settings_portal_id_slug;

	private $settings_form_id_slug;

	private $settings_sub_id_slug;

	private $settings_deactivate_cleanup_slug;

	private $str_ajax_action;

	private $str_nonce_action;

	private $str_nonce_name;

	private $str_fix_validation_errors_msg;

	private $str_error_msg_fallback;

	private $arr_error_msgs;

	private $str_form_field_email;

	private $arr_form_fields_map;

	private $str_script_main_handle;

	private $arr_js_main;

	private $str_script_main_localize_object_name;

	private $arr_localize_script;

	private $arr_context_names;


	/**
	 * Construct the config.
	 *
	 * @param string $plugin_dir The plugin's dir.
	 * @param string $plugin_url The plugin's url.
	 */
	public function __construct( string $plugin_dir, string $plugin_url ) {

		$this->plugin_dir = $plugin_dir;

		$this->plugin_url = $plugin_url;

		$this->setPropertyDefaults();

	}

	/**
	 * Set the various property defaults.
	 *
	 * @return void
	 */
	private function setPropertyDefaults() {

		$this->plugin_slug = array( 'wpezplugins', 'hubspot', 'newsletter', 'form', 'ajax' );
		
		$this->arr_plugin_slugs = array();

		$this->settings_prefix = 'wpezplugins_hnfa_';

		$this->settings_parent_menu_slug = 'tools.php';

		$this->settings_submenu_page_cap = 'manage_options';

		$this->settings_portal_id_slug = 'portal_id';

		$this->settings_form_id_slug = 'form_id';

		$this->settings_sub_id_slug = 'sub_id';

		$this->settings_deactivate_cleanup_slug = 'deactivate_cleanup';

		$this->str_ajax_action = 'wpez_hubspot_newsletter_form_ajax';

		$str_nonce_slug_prefix = 'wpez_hnfa_nonce';

		$this->str_nonce_action = $str_nonce_slug_prefix . '_action_name';

		$this->str_nonce_name = $str_nonce_slug_prefix . '_nonce_name';

		$this->str_fix_validation_errors_msg = '<p>Please address the following:</p>';

		$this->str_error_msg_fallback = 'Error %s: Something is wrong. Please contact the site owner.';

		$str_boilerplate = ' Please refresh the page and try again.';

		$this->arr_error_msgs = array(
			'email'                  => 'A valid email address is required.',
			'consent_process'        => 'Consent to store and process your data is required.',
			'consent_communications' => 'Consent to receive communications is required.',
			'tamper_f'               => 'Error F-333: ' . $str_boilerplate,
			'tamper_c'               => 'Error C-444: ' . $str_boilerplate,
			'tamper_l'               => 'Error L-555: ' . $str_boilerplate,
			'tamper_o'               => 'Error O-777: ' . $str_boilerplate,

			'nonce_failed'           => 'Error NF-111: ' . $str_boilerplate,
			'not_arrays'             => 'Error NA-222: ' . $str_boilerplate,
			'hs_settings_mia'        => 'Error HS-000: Please contact the site owner.',
			'hs_fail'                => 'Error HS-101: ' . $str_boilerplate,
			'hs_email_blocked'       => 'Apparently, your email is on a blacklist. Try a different email address, please.',
			'hs_other'               => 'Error HSO-321: ' . $str_boilerplate,
			'hs_200_never'           => 'Error HSN-200: ' . $str_boilerplate,
			'hs_400_never'           => 'Error HSN-400: ' . $str_boilerplate,
			'hs_other_never'         => 'Error HSN-OTH: ' . $str_boilerplate,
			'curl_post_1'            => 'Error CP-100: Please contact the site owner.',
			'curl_post_2'            => 'Error CP-202: Please contact the site owner.',
			'curl_post_2'            => 'Error CP-202: Please contact the site owner.',
			'spam_true'              => 'Your email address looks like spam. Did you mistype?', 
		);

		$this->str_form_field_email = 'wpez_hsnf_email';

		$this->arr_form_fields_map = array(

			// hs fields

			$this->str_form_field_email                    => array(
				'active'     => true,
				'hs_type'    => 'fields',
				'validate'   => 'email',
				'required'   => 'required',
				'error_type' => 'a',
				'error_code' => 'email',
				'hs_name'    => 'email',
			),

			'wpez_hsnf_name_first' => array(
				'active'     => true,
				'hs_type'    => 'fields',
				'validate'   => 'string',
				'required'   => false,
				'error_type' => 'b',
				'error_code' => 'tamper_f',
				'hs_name'    => 'firstname',
			),

			'wpez_hsnf_name_last' => array(
				'active'     => true,
				'hs_type'    => 'fields',
				'validate'   => 'string',
				'required'   => false,
				'error_type' => 'b',
				'error_code' => 'tamper_f',
				'hs_name'    => 'lastname',
			),

			'wpez_hsnf_form_version'                       => array(
				'active'     => true,
				'hs_type'    => 'fields',
				'validate'   => 'string',
				'required'   => true,
				'error_type' => 'b',
				'error_code' => 'tamper_c',
				'hs_name'    => 'form_version',
			),

			// hs context 

			'pageUri' => array(
				'active'     => true,
				'hs_type'    => 'context',
				'validate'   => 'string',
				'required'   => 'required',
				'error_type' => 'b',
				'error_code' => 'tamper_c',
				'hs_name'    => 'pageUri',
			),

			'pageName' => array(
				'active'     => true,
				'hs_type'    => 'context',
				'validate'   => 'string',
				'required'   => 'required',
				'error_type' => 'b',
				'error_code' => 'tamper_c',
				'hs_name'    => 'pageName',
			),

			'hutk' => array(
				'active'     => true,
				'hs_type'    => 'context',
				'validate'   => 'string',
				'required'   => false,
				'error_type' => 'b',
				'error_code' => 'tamper_c',
				'hs_name'    => 'hutk',
			),

			// hs legal

			'wpez_hsnf_legal_consent_process'              => array(
				'active'     => true,
				'hs_type'    => 'legal',
				'validate'   => 'bool',
				'required'   => 'required',
				'error_type' => 'a',
				'error_code' => 'consent_process',
				'hs_name'    => 'consentToProcess',
			),

			'wpez_hsnf_legal_consent_process_label'        => array(
				'active'     => true,
				'hs_type'    => 'legal',
				'validate'   => 'string',
				'required'   => 'required',
				'error_type' => 'b',
				'error_code' => 'tamper_l',
				'hs_name'    => 'consentToProcess_text',
			),

			'wpez_hsnf_legal_consent_communications'       => array(
				'active'     => true,
				'hs_type'    => 'legal',
				'validate'   => 'bool',
				'required'   => 'required',
				'error_type' => 'a',
				'error_code' => 'consent_communications',
				'hs_name'    => 'communications_value',
			),

			'wpez_hsnf_legal_consent_communications_label' => array(
				'active'     => true,
				'hs_type'    => 'legal',
				'validate'   => 'string',
				'required'   => 'required',
				'error_type' => 'b',
				'error_code' => 'tamper_l',
				'hs_name'    => 'communications_text',
			),

		);
		
		$this->str_script_main_handle = 'wpez-hsnf-main';

		$this->arr_js_main = array(
			'active'    => true,
			'handle'    => $this->str_script_main_handle,
			'src'       => $this->getPluginURL() . '/App/assets/dist/js/wpez-hsnf-main.min.js',
			'deps'      => array(),
			'ver'       => '0.0.0',
			'in_footer' => true,
		);

		$this->str_script_main_localize_object_name = 'aisWPezHSNF';

		$page_uri = 'unknown';
		if ( isset( $_SERVER['HTTP_HOST'], $_SERVER['REQUEST_URI'] ) ) {
			$page_uri = esc_attr( wp_unslash( $_SERVER['HTTP_HOST'] ) . wp_unslash( $_SERVER['REQUEST_URI'] ) );
		}


		$this->arr_script_main_localize = array(
			'active'                  => true,
			'adminAjax'               => admin_url( 'admin-ajax.php' ),
			'ajaxAction'              => $this->str_ajax_action,
			'wpNonceAction'           => $this->str_nonce_action,
			'wpCreateNonce'           => wp_create_nonce( $this->str_nonce_action ),
			'ns'                      => new \stdClass(),
			'qsaForms'                => 'form.wpez-hsnf-form',
			'qsaEmails'               => '.wpez-hsnf-form input[type=email]',
			'qsaOptionals'            => '.wpez-hsnf-step-3 input[name^=wpez_hsnf_name_]',
			'optionalsMinLength'      => array( 0, 0 ),
			'qsStep3Msgs'             => '.wpez-hsnf-step-3-msgs',
			'qsButton'                => 'button[type=submit]',
			'msgSubmit'               => 'Thanks! One moment please...',
			'step2'                   => 'wpez-hsnf-step-2',
			'step3'                   => 'wpez-hsnf-step-3',
			'emailCSSValidContentLen' => 7,
			'waitTime'                => 500,
			'reqNames'                => $this->getRequiredNames( array( 'fields', 'legal' ) ),
			'pageURI'                 => esc_attr( $page_uri ),
			'pageName'                => true,
			'hsUserToken'             => 'hubspotutk',
		);

		$this->arr_context_names = $this->getRequiredNames( array( 'context' ) );
	}

	/**
	 * {@inheritDoc}
	 */
	public function getPluginDir() : string {

		return trim( $this->plugin_dir );
	}

	/**
	 * {@inheritDoc}
	 */
	public function getPluginURL() : string {

		return trim( $this->plugin_url );
	}

	/**
	 * {@inheritDoc}
	 */
	public function getPluginSlug( string $separator = '_' ) : string {

		// Let's not reinvent the wheel.
		$separator = trim( $separator );
		if ( ! empty( $this->arr_plugin_slug[ $separator ] ) ) {
			return $this->arr_plugin_slug[ $separator ];
		}

		// No wheel invented yet? Invent it then.
		if ( '_' === $separator || '-' === $separator ) {
			$this->arr_plugin_slug[ $separator ] = trim( implode( $separator, $this->plugin_slug ) );
			return $this->arr_plugin_slug[ $separator ];
		}

		return __( 'Error: getPluginSlug() $separator must be _ or -.', 'TODO' );
	}

	public function getSettingsPrefix() {

		return $this->settings_prefix;
	}

	public function getSettingsParentMenuSlug() {

		return $this->settings_parent_menu_slug;
	}

	public function getSettingsSubmenuPageCap() {

		return $this->settings_submenu_page_cap;
	}

	public function getSettingsPortalIDName() {

		return $this->settings_prefix . $this->settings_portal_id_slug;
	}

	public function getSettingsPortalID() {

		return get_option( $this->settings_prefix . $this->settings_portal_id_slug );
	}

	public function getSettingsFormIDName() {

		return $this->settings_prefix . $this->settings_form_id_slug;
	}

	public function getSettingsFormID() {

		return get_option( $this->settings_prefix . $this->settings_form_id_slug );
	}

	public function getSettingsSubIDName() {

		return $this->settings_prefix . $this->settings_sub_id_slug;
	}

	public function getSettingsSubID() {

		return get_option( $this->settings_prefix . $this->settings_sub_id_slug );
	}

	public function getSettingsDeactivateCleanupSlug() {

		return $this->settings_prefix . $this->settings_deactivate_cleanup_slug;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getAjaxAction() : string {

		return $this->str_ajax_action;
	}


	/**
	 * {@inheritDoc}
	 */
	public function getNonceAction() : string {

		return $this->str_nonce_action;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getNonceName() : string {

		return $this->str_nonce_name;
	}


	public function getFixValidationErrorsMsg() : string {

		return $this->str_fix_validation_errors_msg;
	}
	

	public function getErrorMsgFallback() : string {

		return $this->str_error_msg_fallback;
	}

	public function getErrorMsgs() : array {

		return $this->arr_error_msgs;
	}

	public function getFormFieldEmail() : string {

		return $this->str_form_field_email;
	}

	public function getFormFieldsMap() : array {

		return $this->arr_form_fields_map;
	}

	public function getScriptMainHandle() : string {

		return $this->str_script_main_handle;
	}

	public function getEnqueueJSMain() : array {

		return $this->arr_js_main;
	}

	public function getScriptMainLocalizeObjectName() : string {

		return $this->str_script_main_localize_object_name;
	}

	public function getScriptMainLocalize() : array {

		$temp = $this->arr_script_main_localize;

		if ( true === $temp['pageName'] ) {
			$temp['pageName'] = \esc_attr( $this->getPageName() );
		}

		return $temp;
	}

	public function getContextNames() {

		return $this->arr_context_names;
	}

	/**
	 * Takes the fields map array and pulls out the active + required for the types specified
	 *
	 * @return array
	 */
	protected function getRequiredNames( array $in_arr = array() ) : array {

		$args_defaults = array(
			'active'   => false,
			'hs_type'  => false,
			'required' => false,
		);

		$arr       = $this->getFormFieldsMap();
		$arr_names = array();
		foreach ( $arr as $name => $args ) {

			if ( ! \is_array( $args ) ) {
				continue;
			}

			$new_args = array_merge( $args_defaults, $args );
			if ( true === $new_args['active'] && in_array( $new_args['hs_type'], $in_arr, true ) && 'required' === $new_args['required'] ) {
				$arr_names[] = esc_attr( $name );
			}
		}
		return $arr_names;
	}


	// TODO - make this into a (simple) class.
	protected function getPageName() {

		global $post;
		if ( is_single() ) {
			return 'Post: ' . $post->post_title;
		} elseif ( is_page() ) {
			return 'Page: ' . $post->post_title;
		} elseif ( is_home() ) {
			return 'Archive + Blog';
		} elseif ( is_archive() ) {
			return 'Archive + ' . wp_strip_all_tags( get_the_archive_title() );
		} elseif ( is_search() ) {
			return 'Search: ' . get_search_query();
		} elseif ( is_404() ) {
			$temp = 'Unknown';
			if ( isset( $_SERVER['REQUEST_URI'] ) ) {
				$temp = wp_unslash( $_SERVER['REQUEST_URI'] );
			}
			return '404: ' . $temp;
		} else {
			return 'Unknown';
		}
	}


}
