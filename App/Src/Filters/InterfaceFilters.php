<?php
/**
 * The interface that defines the methods for customizing various plugin settings / values.
 *
 * @package WPezHNFA\App\Src\Filters
 */

namespace WPezHNFA\App\Src\Filters;

/**
 * Defines the methods for customizing various plugin settings / values. This "bottleneck" enables the plugin to decouple the validation and such from the crux of the code.
 */
interface InterfaceFilters {

	/**
	 * Construct the class.
	 *
	 * @param string $str_plugin_slug The plugin's slug from the config.
	 * @param array  $arr_css_unit The plugin's css unit from the config.
	 */
	public function __construct( string $str_plugin_slug, array $arr_css_unit );



	/**
	 * Undocumented function
	 *
	 * @param array  $arr TODO.
	 * @param string $str TODO.
	 * @param array  $arr2 TODO.
	 *
	 * @return array
	 */
	public function EXAMPLE( array $arr, string $str, array $arr2 ) : array;

}
