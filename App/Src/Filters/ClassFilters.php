<?php

namespace WPezHNFA\App\Src\Filters;

/**
 * Implements InterfaceFilters.
 */
class ClassFilters implements InterfaceFilters {

	/**
	 * The plugin's slug use to "namespace" filter and option names.
	 *
	 * @var string
	 */
	private $str_plugin_slug;

	/**
	 * Array of args for validation of some filter's return.
	 *
	 * @var array
	 */
	private $arr;

	/**
	 * Construct the class.
	 *
	 * @param string $str_plugin_slug The plugin's slug.
	 * @param array  $arr An array of TODO  units (e.g., 'px', 'em', etc.) used for validation.
	 */
	public function __construct( string $str_plugin_slug = '', array $arr = array() ) {

		$this->str_plugin_slug = trim( $str_plugin_slug );
		$this->arr             = $arr;
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $arr TODO.
	 * @param string $str TODO.
	 * @param array  $arr2 TODO.
	 *
	 * @return array
	 */
	public function EXAMPLE( array $arr, string $str, array $arr2 ) : array {

		$temp = apply_filters( $this->str_plugin_slug . '/' . __FUNCTION__, $arr, $str );
		if ( 'validate' === $temp ) {
			return $temp;
		}
		return $arr;
	}

}