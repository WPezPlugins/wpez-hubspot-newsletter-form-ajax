<?php
/**
 * The core functionality of the plugin.
 * 
 * @package WPezHNFA\App\Src
 */

namespace WPezHNFA\App\Src;

use WPezHNFA\App\Config\InterfaceConfig as Config;
use WPezHNFA\App\Src\Filters\InterfaceFilters as Customize;
use WPezHNFA\App\Lib\HubspotFormsSubmission\ClassHubspotFormsSubmission as FormSubmit;




// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

/**
 * The core functionality - methods for the WP filters in ClassPlugin - of the plugin.
 */
class ClassHubspotNewsletterFormAjax {

	/**
	 * Instance of the config class.
	 *
	 * @var object
	 */
	private $new_config;

	/**
	 * Instance of the customize class.
	 *
	 * @var object
	 */
	private $new_customize;

	private $arr_form_fields_map;

	private $arr_error_msgs;

	private $str_error_fallback;

	/**
	 * Construct the class.
	 *
	 * @param Config $new_config    $new_config Instance of the config class.
	 * @param Customize $new_customize $new_core_image_customize Instance of the customize class.
	 */
	public function __construct( Config $new_config, Customize $new_customize ) {

		$this->new_config    = $new_config;
		$this->new_customize = $new_customize;
		$this->setPropertyDefaults();
	}


	/**
	 * Sets the defaults for the properties.
	 *
	 * @return void
	 */
	private function setPropertyDefaults() {

		$this->arr_form_fields_map = $this->new_config->getFormFieldsMap();
		$this->arr_error_msgs      = $this->new_config->getErrorMsgs();
		$this->str_error_fallback  = $this->new_config->getErrorMsgFallback();
	}

	/**
	 * The primary method for the ajax request.
	 *
	 * @return string
	 */
	public function wpAjaxCallback() {

		$str_nonce_action = trim( $this->new_config->getNonceAction() );

		if ( ! isset( $_POST[ $str_nonce_action ] )
		|| ! wp_verify_nonce( $_POST[ $str_nonce_action ], $str_nonce_action ) ) {

			$wp_sj_resp = array(
				'status'  => 'error',
				'message' => $this->makeMsg( 'nonce_failed', 'FB-NF-111' ),
			);
			wp_send_json( $wp_sj_resp, 200 );

		}

		if ( ! is_array( $this->arr_form_fields_map ) || ! is_array( $this->arr_error_msgs ) ) {

			$wp_sj_resp = array(
				'status'  => 'error',
				'message' => $this->makeMsg( 'not_arrays', 'FB-NA-222' ),
			);
			wp_send_json( $wp_sj_resp, 200 );
		}

		// For testing.
		// unset( $_POST['wpez_hsnf_email']);
		// unset( $_POST['wpez_hsnf_legal_consent_process']);

		$arr_defaults = array(
			'active'     => false,
			'hs_type'    => false,
			'validate'   => 'string',
			'required'   => 'required',
			'error_type' => 'b',  // type 'a' will show individual errors (for form field), type 'b' are more or less errors related to the form being tampered with prior to submit
			'error_code' => 'tamper_o',
			'hs_name'    => false,
		);

		$arr_fields       = array();
		$arr_fields_assoc = array();
		$arr_values       = array();
		$arr_errors       = array();
		foreach ( $this->arr_form_fields_map as $name => $arr_args ) {

			// Not an array? Continue!
			if ( ! is_array( $arr_args ) ) {
				continue;
			}

			$arr_args = array_merge( $arr_defaults, $arr_args );

			if ( true !== $arr_args['active'] ) {
				continue;
			}

			if ( false === $arr_args['required'] && ( ! isset( $_POST[ $name ] ) || empty( $_POST[ $name ] ) ) ) {
				continue;
			}

			// Is a required set and ! empty?
			if ( 'required' === $arr_args['required'] && ( ! isset( $_POST[ $name ] ) || empty( $_POST[ $name ] ) ) ) {

				$arr_errors[ $arr_args['error_type'] ][ $arr_args['error_code'] ] = true;
				continue;
			}

			switch ( $arr_args['validate'] ) {

				case 'email':
					if ( ! filter_var( $_POST[$name], FILTER_VALIDATE_EMAIL ) ) {
						$arr_errors[ $arr_args['error_type'] ][ $arr_args['error_code'] ] = true;
						continue 2;
					}
					break;

				case 'bool':
					if ( ! is_string( $_POST[ $name ] ) ) {
						$arr_errors[ $arr_args['error_type'] ][ $arr_args['error_code'] ] = true;
						continue 2;
					}
					break;

				case 'string':
				default:
					if ( ! is_string( $_POST[ $name ] ) ) {
						$arr_errors[ $arr_args['error_type'] ][ $arr_args['error_code'] ] = true;
						continue 2;
					}
					break;
			}

			if ( 'fields' === trim( $arr_args['hs_type'] ) ) {

					$arr_fields[] = array(
						'name'  => $arr_args['hs_name'],
						'value' => wp_unslash( $_POST[ $name ] ),
					);

					$arr_fields_assoc[ $arr_args['hs_name'] ] = wp_unslash( $_POST[ $name ] );
			} else {

				$arr_values[ $arr_args['hs_name'] ] = wp_unslash( $_POST[ $name ] );
			}
		}

		$ret_err_msg = true;
		// "Type A" errors are typically for non-hidden fields. 
		if ( isset( $arr_errors['a'] ) && \is_array( $arr_errors['a'] ) ) {

			$ret_err_msg = '<ul>';
			foreach( $arr_errors['a'] as $err_code => $status ) {

				$str_err      = $this->makeMsg( $err_code, 'FB-SSV-888' );
				$ret_err_msg .= '<li>' . esc_html( $str_err ) . '</li>';
			}
			$ret_err_msg .= '</ul>';
			$ret_err_msg =  \wp_kses_post( $this->new_config->getFixValidationErrorsMsg() ) . $ret_err_msg;

		} elseif ( isset( $arr_errors['b'] ) && \is_array( $arr_errors['b'] ) ) {

			// For type "b" error we'll only list the first msg. Why? Because, odds are the page
			// was tampered with, and a refresh - and no more tampering - will clear up everything "b".
			$err_b       = array_key_first( $arr_errors['b'] );
			$ret_err_msg = $this->makeMsg( $err_b, 'FB-ERR-B888' );
		}

		// if we have a string then we have (validation) errors to return
		if ( is_string( $ret_err_msg ) ) {
			$wp_sj_resp = array(
				'status'  => 'error',
				'message' => $ret_err_msg,
			);
			wp_send_json( $wp_sj_resp, 200 );
		}

		// Filter to allow for a spam check.
		$arr_spam_check = array(
			'active'  => false,
			'is_spam' => false,
		);
		$arr_resp_spam_check = apply_filters( __NAMESPACE__ . '\\' . __FUNCTION__ . '\spamCheck', $arr_spam_check, $arr_fields_assoc );
		
		// Gotta get an array back, else use our defaults.
		if ( is_array( $arr_resp_spam_check ) ) {
			$arr_resp_spam_check = array_merge( $arr_spam_check, $arr_resp_spam_check );
		} else {
			$arr_resp_spam_check = $arr_spam_check;
		}

		// is spam checking active?
		$spam_check_active = 'false';
		if ( false !== $arr_resp_spam_check['active'] ) {
			$spam_check_active = 'true';
		}
		$arr_fields[] = array(
			'name'  => 'spam_check_active',   // TODO - add to Config. 
			'value' => $spam_check_active,
		);

		// if the spam check is active, then also check the 'is_spam' arg.
		if ( 'true' === $spam_check_active && true === $arr_resp_spam_check['is_spam'] ) {
			// true means it's spam.
			$wp_sj_resp = array(
				'status'  => 'error',
				'message' => $this->makeMsg( 'spam_true', 'FB-SPM-TRU' ),
			);
			wp_send_json( $wp_sj_resp, 200 );
		}

		// Wow! We've made it!! Submit to Hubspot!!!
		$hs_response = $this->hsFormSubmit( $arr_fields, $arr_values );
		$this->decodeHSResponse( $hs_response );
	}

	/**
	 * Make the response message.
	 *
	 * @param string $str_key The error msg key.
	 * @param string $fb_code If a value for the key doesn't exist, the fallback code.
	 *
	 * @return string
	 */
	protected function makeMsg( string $str_key, string $fb_code = 'FB-X-000' ) : string {

		if ( isset( $this->arr_error_msgs[ $str_key ] ) ) {
			return $this->arr_error_msgs[ $str_key ];
		}

		return sprintf( $this->str_error_fallback, $fb_code );
	}

	/**
	 * Submit the form to Hubspot. https://legacydocs.hubspot.com/docs/methods/forms/submit_form
	 *
	 * @param array $arr_fields The HS fields.
	 * @param array $arr_values The values from the form.
	 *
	 * @return string
	 */
	protected function hsFormSubmit( array $arr_fields = array(), array $arr_values = array() ) {

		$portal_id = $this->new_config->getSettingsPortalID();
		$form_id   = $this->new_config->getSettingsFormID();
		$sub_id    = $this->new_config->getSettingsSubID();

		// TODO - Type validation (not just empty)
		if ( empty( $portal_id ) || empty( $form_id ) || empty( $sub_id ) ) {

				$wp_sj_resp = array(
					'status'  => 'error',
					'message' => $this->makeMsg( 'hs_settings_mia', 'FB-HS-000' ),
				);
				wp_send_json( $wp_sj_resp, 200 );
		}

		$new_hsfs = new FormSubmit( $portal_id, $form_id );

		// fields.
		$new_hsfs->pushArgs( 'fields', $arr_fields );

		// context .
		$context = array();
		foreach ( $this->new_config->getContextNames() as $ndx => $str_key ) {
			if ( isset( $arr_values[ $str_key ] ) && ! empty( $arr_values[ $str_key ] ) ) {
				$context[ $str_key ] = \esc_attr( $arr_values[ $str_key ] );
			}
		}

		$new_hsfs->pushArgs( 'context', $context );

		// legal.
		$consent_to_process_text = 'consentToProcess > text > default';
		if ( isset( $arr_values['consentToProcess_text'] ) && ! empty( $arr_values['consentToProcess_text'] ) ) {
			$consent_to_process_text = preg_replace( "/\r|\n|\t/", '', $arr_values['consentToProcess_text'] );
		}

		$communications_text = 'communications > text > default';
		if ( isset( $arr_values['communications_text'] ) && ! empty( $arr_values['communications_text'] ) ) {
			$communications_text = preg_replace( "/\r|\n|\t/", '', $arr_values['communications_text'] );
		}

		// Include this object when GDPR options are enabled.
		$legal = array(
			'consent' => array(
				'consentToProcess' => true,
				'text'             => 'I agree to allow Example Company to store and process...', // '123' . $consent_to_process_text,
				'communications'   => array(
					array(
						'value'              => true,
						'subscriptionTypeId' => trim( $sub_id ),
						'text'               => $communications_text,
					),
				),
			),
		);

		$new_hsfs->pushArgs( 'legalConsentOptions', $legal );

		$arr_curl_resp = $new_hsfs->curlPost();

		return $arr_curl_resp;

	}

	/**
	 * Decode the HS response and "unify it" with our other (validation) replies.
	 *
	 * @param array $arr_hs_resp
	 *
	 * @return string
	 */
	protected function decodeHSResponse( $arr_hs_resp ) {

		if ( ! isset( $arr_hs_resp['status_code'] ) || ! isset( $arr_hs_resp['response'] ) ) {

				$wp_sj_resp = array(
					'status'  => 'error',
					'message' => $this->makeMsg( 'hs_fail', 'FB-HS-101' ),
				);
				wp_send_json( $wp_sj_resp, 200 );
		}

		switch ( $arr_hs_resp['status_code'] ) {

			case 200:
				$jd_resp = json_decode( $arr_hs_resp['response'], true );

				if ( isset( $jd_resp['inlineMessage'] ) ) {

					$wp_sj_resp = array(
						'status'  => 'success',
						'message' => $jd_resp['inlineMessage'],
					);
					wp_send_json( $wp_sj_resp, 200 );

				} elseif ( isset( $jd_resp['redirectUri'] ) ) {

					$wp_sj_resp = array(
						'status'  => 'redirect',
						'message' => esc_url( $jd_resp['redirectUri'] ),
					);
					wp_send_json( $wp_sj_resp, 200 );

				} else {
					$wp_sj_resp = array(
						'status'  => 'error',
						'message' => $this->makeMsg( 'hs_200_never', 'FB-HSN-200' ),
					);
					wp_send_json( $wp_sj_resp, 200 );
				}
				break;

			case 400:
				$jd_resp = json_decode( $arr_hs_resp['response'], true );

				if ( isset( $jd_resp['errors'][0]['errorType'] ) ) {

					switch ( $jd_resp['errors'][0]['errorType'] ) {

						case 'CURL_POST_1':
							$wp_sj_resp = array(
								'status'  => 'error',
								'message' => $this->makeMsg( 'curl_post_1', 'FB-CP-100' ),
							);
							wp_send_json( $wp_sj_resp, 200 );
							break;

						case 'CURL_POST_2':
							$wp_sj_resp = array(
								'status'  => 'error',
								'message' => $this->makeMsg( 'curl_post_2', 'FB-CP-100' ),
							);
							wp_send_json( $wp_sj_resp, 200 );
							break;							

						case 'INVALID_EMAIL':
							$wp_sj_resp = array(
								'status'  => 'error',
								'message' => $this->makeMsg( 'email', 'FB-HSEF' ),
							);
							wp_send_json( $wp_sj_resp, 200 );
							break;

						case 'BLOCKED_EMAIL':
							$wp_sj_resp = array(
								'status'  => 'error',
								'message' => $this->makeMsg( 'hs_email_blocked', 'FB-EBLK' ),
							);
							wp_send_json( $wp_sj_resp, 200 );
							break;

						default:
							$wp_sj_resp = array(
								'status'  => 'error',
								'message' => $this->makeMsg( 'hs_other', 'FB-HSO-321' ),
							);
							wp_send_json( $wp_sj_resp, 200 );
					}
				} else {

					$wp_sj_resp = array(
						'status'  => 'error',
						'message' => $this->makeMsg( 'hs_400_never', 'FB-HSN-400' ),
					);
					wp_send_json( $wp_sj_resp, 200 );

				}
				break;

			default:
				$wp_sj_resp = array(
					'status'  => 'error',
					'message' => $this->makeMsg( 'hs_other_never', 'FB-HSN-OTH' ),
				);
				wp_send_json( $wp_sj_resp, 200 );
		}

	}

}
