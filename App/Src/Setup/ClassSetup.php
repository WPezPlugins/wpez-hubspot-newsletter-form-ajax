<?php

namespace WPezHNFA\App\Src\Setup;

use WPezHNFA\App\Config\InterfaceConfig;

/**
 * TODO
 */
class ClassSetup {

	protected $new_config;

	public function __construct( InterfaceConfig $new_config ) {

		$this->new_config = $new_config;
	}


	/**
	 * TODO add the ez enqueue class to core and use that.
	 *
	 * @return void
	 */
	public function enqueueJSMain() {

		$arr = $this->new_config->getEnqueueJSMain();

		if ( true !== $arr['active'] || ! is_array( $arr ) ) {
			return;
		}

		\wp_enqueue_script(
			$arr['handle'],
			$arr['src'],
			$arr['deps'],
			$arr['ver'],
			$arr['in_footer'],
		);
	}


	/**
	 * TODO 
	 *
	 * @return void
	 */
	public function localizeJSMain() {

		wp_add_inline_script( 
			$this->new_config->getScriptMainHandle(),
			'const ' . \esc_attr( $this->new_config->getScriptMainLocalizeObjectName() ) . ' = ' . json_encode( $this->new_config->getScriptMainLocalize() ),
			'before'
		);
	}


}