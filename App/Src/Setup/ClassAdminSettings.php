<?php
/**
 * @package WPezHNFA\App\Src\Setup;
 */

namespace WPezHNFA\App\Src\Setup;

use WPezHNFA\App\Config\InterfaceConfig;

/**
 * Admin settings
 */
class ClassAdminSettings {

	/**
	 * Instance of the plugin's config class.
	 *
	 * @var object
	 */
	protected $new_config;


	/**
	 * A string to prefix some strings; a namespace of sorts.
	 *
	 * @var string
	 */
	protected $prefix;


	/**
	 * The parent value for the add_submenu_page().
	 *
	 * @var string
	 */
	protected $parent_slug;


	/**
	 * Used in add_settings_section() and add_settings_field().
	 *
	 * @var string
	 */
	protected $settings_slug;


	/**
	 * Used in settings_fields(), add_settings_section() and add_settings_field().
	 *
	 * @var string
	 */
	protected $section_slug;


	/**
	 * The option name that stores the HS portal ID.
	 *
	 * @var string
	 */
	protected $portal_id_name;


	/**
	 * The option name that stores the HS form ID.
	 *
	 * @var string
	 */
	protected $form_id_name;

	/**
	 * Subscription Type ID
	 *
	 * @var string
	 */
	protected $sub_id_name;


	/**
	 * The option name that stores the checkbox value to clean up on deactivate.
	 *
	 * @var string
	 */
	protected $deactivate_cleanup_name;

	/**
	 * Let's construct.
	 *
	 * @param InterfaceConfig $new_config
	 */
	public function __construct( InterfaceConfig $new_config ) {

		$this->new_config = $new_config;

		$this->setPropertyDefaults();
	}


	/**
	 * A method for setting defaults of the class' properties.
	 *
	 * @return void
	 */
	protected function setPropertyDefaults() {

		$this->prefix                  = $this->new_config->getSettingsPrefix();
		$this->parent_slug             = $this->new_config->getSettingsParentMenuSlug();
		$this->settings_slug           = $this->prefix . 'settings';
		$this->section_slug            = $this->prefix . 'section';
		$this->portal_id_name          = $this->new_config->getSettingsPortalIDName();
		$this->form_id_name            = $this->new_config->getSettingsFormIDName();
		$this->sub_id_name             = $this->new_config->getSettingsSubIDName();
		$this->deactivate_cleanup_name = $this->new_config->getSettingsDeactivateCleanupSlug();
	}

	/**
	 * The callable function for the register_activation_hook.
	 *
	 * @return void
	 */
	public function activate() {
		// TODO.
	}


	/**
	 *  The callable function for the register_deactivation_hook
	 *
	 * @return void
	 */
	public function deactivate() {

		// TODO - move get value to config?
		$deactivate_cleanup = trim( get_option( $this->deactivate_cleanup_name ), '' );

		if ( ! empty( $deactivate_cleanup ) ) {

			delete_option( $this->portal_id_name );
			delete_option( $this->form_id_name );
			delete_option( $this->deactivate_cleanup_name );
		}	
	}

	/**
	 * Add the submenu page.
	 *
	 * @return void
	 */
	public function addSubmenuPage() {

		// TODO - add filter to allow this to be customized?
		$funct = array( $this, 'settings' );

		add_submenu_page(
			$this->parent_slug,
			__( 'Settings: WPezPlugin - Hubspot Newsletter Form Ajax', 'wpez-hnf' ),
			__( 'WPez HNFA', 'wpez-hnfa' ),
			$this->new_config->getSettingsSubmenuPageCap(),
			$this->new_config->getPluginSlug( '-' ),
			$funct,
		);
	}


	/**
	 * The settings for the submenu page.
	 *
	 * @return void
	 */
	public function settings() {
		?>
		<div class="wrap">
		<h1><?php echo esc_attr( __( 'Settings: WPezPlugins - Hubspot Newsletter Form Ajax', 'wpez-hnfa' ) ); ?></h1>
			<form method="post" action="options.php">
			<?php
				settings_fields( $this->section_slug );

				do_settings_sections( $this->settings_slug );

				submit_button( __( 'Save Settings', 'wpez-hnfa' ) );
			?>
			</form>
		</div>
		<?php
	}


	/**
	 * Displays the settings using add_settings_section(), add_settings_field(), and register_setting;
	 *
	 * @return void
	 */
	public function displaySettings() {

		// https://developer.wordpress.org/reference/functions/add_settings_section/
		add_settings_section(
			$this->section_slug,
			__( 'Instructions', 'wpez-hsnf' ),
			array( $this, 'display_header_options_content' ),
			$this->settings_slug,
		);

		// https://developer.wordpress.org/reference/functions/add_settings_field/
		add_settings_field(
			$this->portal_id_name,
			__( 'Portal ID *', 'wpez-hnfa' ),
			array( $this, 'hs_portal_id' ),
			$this->settings_slug,
			$this->section_slug,
			array( 'label_for' => $this->portal_id_name ),
		);

		add_settings_field(
			$this->form_id_name,
			__( 'Form ID *', 'wpez-hnfa' ),
			array( $this, 'hs_form_id' ),
			$this->settings_slug,
			$this->section_slug,
			array( 'label_for' => $this->form_id_name ),
		);

		add_settings_field(
			$this->sub_id_name,
			__( 'Subscription Type ID *', 'wpez-hnfa' ),
			array( $this, 'hs_sub_id' ),
			$this->settings_slug,
			$this->section_slug,
			array( 'label_for' => $this->sub_id_name ),
		);

		add_settings_field(
			$this->deactivate_cleanup_name,
			__( 'Clean up on deactivate?', 'wpez-hnfa' ),
			array( $this, 'deactivate_cleanup' ),
			$this->settings_slug,
			$this->section_slug,
			array( 'label_for' => $this->deactivate_cleanup_name ),
		);

		// https://developer.wordpress.org/reference/functions/register_setting/
		register_setting(
			$this->section_slug,
			$this->portal_id_name,
			array( 'sanitize_callback' => array( $this, 'sanitize_ctype_digit' ) ),
		);

		register_setting(
			$this->section_slug,
			$this->form_id_name,
			array( 'sanitize_callback' => array( $this, 'sanatize_form_id' ) ),
		);

		register_setting(
			$this->section_slug,
			$this->sub_id_name,
			array( 'sanitize_callback' => array( $this, 'sanitize_ctype_digit' ) ),
		);		

		register_setting(
			$this->section_slug,
			$this->deactivate_cleanup_name,
			array( 'sanitize_callback' => array( $this, 'sanitize_check' ) ),
		);
	}


	/**
	 * Sanitation callback for the HS portal_id.
	 *
	 * @param string $val The value from the form.
	 *
	 * @return string
	 */
	public function sanitize_ctype_digit( string $val ) : string {

		if ( ctype_digit( $val ) ) {
			return $val;
		}
		return '';
	}

	/**
	 * Sanitation callback for the HS form_id.
	 *
	 * @param string $val The value from the form.
	 *
	 * @return string
	 */
	public function sanatize_form_id( string $val ) : string {

		$val = trim( $val );

		// a similar pattern is used for the html5 input for the form_id.
		$pattern = '/^([a-z0-9\-]+)$/';
		if ( preg_match( $pattern, $val ) ) {
			return $val;
		}
		return '';
	}


	/**
	 * Sanitize callback for the checkbox.
	 *
	 * @param string $val The value from the form.
	 * @return string
	 */
	public function sanitize_check( $val ) {

		if ( '1' === $val ) {
			return $val;
		}
		return '';
	}

	/**
	 * Display the settings API's settings section header.
	 *
	 * @return void
	 */
	public function display_header_options_content() {
		echo __( 'All Hubspot fields are required.', 'wpez-hnfa' );
	}

	/**
	 * Render the input for: portal_id.
	 *
	 * @return void
	 */
	public function hs_portal_id() {
		?>
			<input type="number" step="1" pattern="\d+" name="<?php echo \esc_attr( $this->portal_id_name ); ?>" id="wpez_hnfa_portal_id" value="<?php echo esc_attr( get_option( $this->portal_id_name ) ); ?>" />
		<?php
		echo '<p>' . esc_attr( __( 'An integer, of 8 digits or so.', 'wpez-hnfa' ) );

	}

	/**
	 * Render the input for: form id
	 *
	 * @return void
	 */
	public function hs_form_id() {
		?>
		<input type="text" pattern="^([a-z0-9\-]+)$" name="<?php echo \esc_attr( $this->form_id_name ); ?>" id="wpez_hnfa_form_id" class="regular-text" value="<?php echo esc_attr( get_option( $this->form_id_name ) ); ?>" />
		<?php
		echo '<p>' . esc_attr( __( 'A multi-segment string of letters, numbers, and dashes.', 'wpez-hnfa' ) );
		echo '<br>' . esc_attr( __( 'Be sure to remove leading and trailing spaces.', 'wpez-hnfa' ) ) . '</p>';
	}


	/**
	 * Render the input for: portal_id.
	 *
	 * @return void
	 */
	public function hs_sub_id() {
		?>
			<input type="number" step="1" pattern="\d+" name="<?php echo \esc_attr( $this->sub_id_name ); ?>" id="wpez_hnfa_sub_id" value="<?php echo esc_attr( get_option( $this->sub_id_name ) ); ?>" />
		<?php
		echo '<p>' . esc_attr( __( 'Another integer, of 8 digits or so.', 'wpez-hnfa' ) );

	}	

	/**
	 *  Render the input for: deactivate_cleanup.
	 *
	 * @return void
	 */
	public function deactivate_cleanup() {
		?>
		<!-- Here we are comparing stored value with 1. Stored value is 1 if user checks the checkbox otherwise empty string. -->
		<input type="checkbox" name="<?php echo \esc_attr( $this->deactivate_cleanup_name ); ?>" id="<?php echo \esc_attr( $this->deactivate_cleanup_name ); ?>" value="1" <?php checked( 1, get_option( $this->deactivate_cleanup_name ), true ); ?> />
		<?php
		echo '<span>' . esc_attr( __( 'Check to delete the plugin settings when deactivating the plugin.', 'wpez-hnfa' ) ) . '</span>';
	} 
}
